package com.example.cotizacionautos;

import java.io.Serializable;
import java.util.Random;
public class Cotizacion implements Serializable {

    private String cliente;
    private int folio;
    private String descripcion;
    private float valorAuto;

    private float porEnganche;
    private int plazos;

    public Cotizacion(int folio, String descripcion, String cliente, float valorAuto, float porEnganche, int plazos) {
        this.folio = folio;
        this.cliente=cliente;
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porEnganche = porEnganche;
        this.plazos = plazos;
    }

    public Cotizacion(){
        this.folio = 0;
        this.descripcion = "";
        this.valorAuto = 0;
        this.porEnganche = 0;
        this.plazos = 0;
        this.cliente="";
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public float getPorEnganche() {
        return porEnganche;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }

    public int getPlazos() {
        return plazos;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }

    public int genrarFolio(){
        int folio = 0;
        Random r = new Random();
        folio = r.nextInt()%1000;
        return folio;
    }

    public float calcularPagoInicial(){
        float enganche=0;
        enganche = this.valorAuto * (this.porEnganche/100f);
        return enganche;
    }

    public float calcularPagoMensual(){
        float pagoMensual =0;
        pagoMensual = (this.getValorAuto()-this.calcularPagoInicial())/this.plazos;
        return pagoMensual;
    }



}

