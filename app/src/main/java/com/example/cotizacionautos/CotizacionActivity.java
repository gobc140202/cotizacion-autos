package com.example.cotizacionautos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class CotizacionActivity extends AppCompatActivity {
    private TextView tvCliente,tvFolio;
    private EditText descAuto,valorAuto,porcentaje;

    private Button btnRealizarVenta,btnCerrar,btnLimpiar;

public void iniciarObjetos(){
    btnRealizarVenta =findViewById(R.id.btnRealizarVenta);
    descAuto=findViewById(R.id.txtDescripcion);
    valorAuto=findViewById(R.id.txtValorAuto);
    porcentaje=findViewById(R.id.txtPorcentaje);
    tvCliente=findViewById(R.id.lblNombreCliente);
    tvFolio=findViewById(R.id.lblFolio);
    btnCerrar=findViewById(R.id.btnCerrar);
    btnLimpiar=findViewById(R.id.btnLimpiar);
}
    private int obtenerPlazosSeleccionados() {
        int plazos = 0;

        RadioButton rdb12 = findViewById(R.id.rdb12);
        RadioButton rdb18 = findViewById(R.id.rdb18);
        RadioButton rdb24 = findViewById(R.id.rdb24);
        RadioButton rdb36 = findViewById(R.id.rdb36);

        if (rdb12.isChecked()) {
            plazos = 12;
        } else if (rdb18.isChecked()) {
            plazos = 18;
        } else if (rdb24.isChecked()) {
            plazos = 24;
        } else if (rdb36.isChecked()) {
            plazos = 36;
        }
        return plazos;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);

        iniciarObjetos();
        Cotizacion cotizacion = new Cotizacion();

        String cliente = getIntent().getStringExtra("cliente");
        int folio = cotizacion.genrarFolio();

        tvCliente.setText("Cliente: " + cliente);
        tvFolio.setText("Folio: " + folio);

        btnRealizarVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (descAuto.getText().toString().equals("")||valorAuto.getText().toString().equals("")||porcentaje.getText().toString().equals("")){
                    Toast.makeText(CotizacionActivity.this, "Complete el campo de descripción antes de continuar", Toast.LENGTH_SHORT).show();}
               else {


                    Cotizacion cotizacion = new Cotizacion();
                    cotizacion.setDescripcion(descAuto.getText().toString());
                    cotizacion.setValorAuto(Float.parseFloat(valorAuto.getText().toString()));
                    cotizacion.setPorEnganche(Float.parseFloat(porcentaje.getText().toString()));
                    cotizacion.setPlazos(obtenerPlazosSeleccionados());
                    cotizacion.setFolio(folio);
                    cotizacion.setCliente(cliente);

                    Intent intento = new Intent(CotizacionActivity.this, TicketActivity.class);
                    intento.putExtra("cotizacion", cotizacion);
                    startActivity(intento);

                    tvCliente.setText("");
                    tvFolio.setText("");
                    descAuto.setText("");
                    valorAuto.setText("");
                    porcentaje.setText("");
                    finish();
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intento=new Intent(CotizacionActivity.this, MainActivity.class);
                startActivity(intento);
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descAuto.setText("");
                valorAuto.setText("");
                porcentaje.setText("");
            }
        });

    }
}