package com.example.cotizacionautos;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public EditText txtCliente;
    private Button btnRealizarCotizacion,btnCerrar;

    public void iniciarObjetos(){
        txtCliente=findViewById(R.id.txtCliente);
        btnCerrar=findViewById(R.id.btnSalir);
        btnRealizarCotizacion=findViewById(R.id.btnCotizacion);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarObjetos();

        btnRealizarCotizacion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (txtCliente.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, "Ingrese el nombre del cliente antes de continuar", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intento = new Intent(MainActivity.this, CotizacionActivity.class);
                    intento.putExtra("cliente", txtCliente.getText().toString());
                    startActivity(intento);
                    finish();
                }
                }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}