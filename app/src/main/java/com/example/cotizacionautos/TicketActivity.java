package com.example.cotizacionautos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TicketActivity extends AppCompatActivity {
    private TextView tvCliente,tvFolio,descAuto,valorAuto,porcentaje,plazos,pagoInicial,pagoMensual,mensaje;
    private Button realizarNuevaVenta,btnCerrar;

    public void  iniciarObjetos(){
        btnCerrar=findViewById(R.id.btnCerrar);
        mensaje=findViewById(R.id.mensaje);
        tvCliente = findViewById(R.id.lblNombreCliente);
        tvFolio = findViewById(R.id.lblFolio);
        descAuto = findViewById(R.id.textViewDescripcion);
        valorAuto = findViewById(R.id.textViewValorAuto);
        porcentaje = findViewById(R.id.textViewPorcentaje);
        plazos = findViewById(R.id.textViewPlazos);
        pagoInicial = findViewById(R.id.textViewPagoInicial);
        pagoMensual = findViewById(R.id.textViewPagoMensual);
        realizarNuevaVenta=findViewById(R.id.btnRealizarNuevaVenta);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);

        iniciarObjetos();
        Intent intent = getIntent();
        Cotizacion cotizacion = (Cotizacion) intent.getSerializableExtra("cotizacion");

        tvCliente.setText("Cliente: " + cotizacion.getCliente());
        tvFolio.setText("Folio: " + cotizacion.getFolio());
        descAuto.setText("Descripción: " + cotizacion.getDescripcion());
        valorAuto.setText("Valor Auto: " + cotizacion.getValorAuto());
        porcentaje.setText("Porcentaje: " +cotizacion.getPorEnganche()+"%");
        plazos.setText("Plazos: " + cotizacion.getPlazos() + " Meses");
        pagoInicial.setText("Pago Inicial: $" + cotizacion.calcularPagoInicial());
        pagoMensual.setText("Pago Mensual: $" + cotizacion.calcularPagoMensual());

        mensaje.setText("¡¡¡GRACIAS POR SU COMPRA "+cotizacion.getCliente()+" !!!!");

        realizarNuevaVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intento= new Intent(TicketActivity.this,MainActivity.class);
                startActivity(intento);
                finish();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}